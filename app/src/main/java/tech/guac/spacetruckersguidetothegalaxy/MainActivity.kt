package tech.guac.spacetruckersguidetothegalaxy

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*
import tech.guac.spacetruckersguidetothegalaxy.adapters.CategoryAdapter
import tech.guac.spacetruckersguidetothegalaxy.model.Category
import tech.guac.spacetruckersguidetothegalaxy.model.CategoryType

class MainActivity : AppCompatActivity() {

    lateinit var recyclerView: RecyclerView

//    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
//        when (item.itemId) {
//            R.id.navigation_shipyard -> {
//                // filtering category list to relevant tabs
//                recyclerView.adapter = CategoryAdapter(this, Category.categoryList.filter { cat -> cat.categoryType == CategoryType.SHIPYARD}, R.layout.category_layout)
//                return@OnNavigationItemSelectedListener true
//            }
//            R.id.navigation_commodities -> {
//                recyclerView.adapter = CategoryAdapter(this, Category.categoryList.filter { cat -> cat.categoryType == CategoryType.COMMODITIES}, R.layout.category_layout)
//                return@OnNavigationItemSelectedListener true
//            }
//
//            R.id.navigation_outfitting -> {
//                recyclerView.adapter = CategoryAdapter(this, Category.categoryList.filter { cat -> cat.categoryType == CategoryType.OUTFITTING}, R.layout.category_layout)
//                return@OnNavigationItemSelectedListener true
//            }
//
//            R.id.navigation_about -> {
//                return@OnNavigationItemSelectedListener true
//            }
//        }
//        false
//    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        recyclerView = findViewById(R.id.ship_manufacturers)
        recyclerView.layoutManager = LinearLayoutManager(this)

    }
}