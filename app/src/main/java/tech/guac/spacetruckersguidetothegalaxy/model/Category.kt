package tech.guac.spacetruckersguidetothegalaxy.model

/**
 * # Category
 * A category
 *
 * @author Brandon Phillips
 * @since ADDER 0.1
 */
class Category(val categoryName: String, val categoryType: CategoryType, val imageResourceName: String) {
    companion object {
        // pre-populating static list of categories.
        val categoryList = listOf<Category>(
                Category("Small Ships",CategoryType.SHIPYARD, "ship_sidewinder"),
                Category("Medium Ships",CategoryType.SHIPYARD, "ship_ferdelance"),
                Category("Large Ships",CategoryType.SHIPYARD, "ship_anaconda"),
                Category("Fighters",CategoryType.SHIPYARD, "ship_eagle")
        )
    }
}

enum class CategoryType {
    SHIPYARD,
    COMMODITIES,
    OUTFITTING
}
