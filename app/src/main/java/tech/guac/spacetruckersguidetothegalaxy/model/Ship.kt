package tech.guac.spacetruckersguidetothegalaxy.model

import org.w3c.dom.Text

/**
 * # Ship
 * Ship class storing data on important ship info, such as ship size, ship manufacturer and make,
 * weapon and utility slots, and galactic average credit price.
 *
 * @author Brandon Phillips
 * @since ADDER 0.1
 */
data class Ship(
        val shipName: String,
        val shipManufacturer: ShipManufacturer,
        val shipSize: ShipSize,
        val smlHardpointCount: Int,
        val medHardpointCount: Int,
        val lrgHardpointCount: Int,
        val hgeHardpointCount: Int,
        val utilMountCount: Int,
        val avgGalacticCost: Long,
        val shipDescription: String
) {
    // similar to 'static' n Java
    companion object {
        val shipList: ArrayList<Ship> = ArrayList()
    }
}

/**
 * # Ship Size
 * Enum declaring ship size of a [Ship].
 *
 *  @author Brandon Phillips
 * @since ADDER 0.1
 */
enum class ShipSize {
    SMALL,
    MEDIUM,
    LARGE,
    FIGHTER
}

/**
 * # Ship Manufacturer
 * Enum declaring the manufacturer of a [Ship].
 *
 * @author Brandon Phillips
 * @since ADDER 0.1
 */
enum class ShipManufacturer(val fullName: String) {
    DELACY("Faulcon DeLacy"),
    CORE("Core Dynamics"),
    ZORGON("Zorgon Peterson"),
    LAKON("Lakon Spaceways"),
    KRUGER("Saud Kruger"),
    GUATAMAYA("Guatamaya");
}